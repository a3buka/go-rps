package modules

import (
	"gitlab.com/a3buka/go-rpc/auth/internal/infrastructure/client"
	"gitlab.com/a3buka/go-rpc/auth/internal/infrastructure/component"
	aservice "gitlab.com/a3buka/go-rpc/auth/internal/modules/auth/service"
	uservice "gitlab.com/a3buka/go-rpc/auth/internal/modules/user/service"
	"gitlab.com/a3buka/go-rpc/auth/internal/storages"
	"gitlab.com/a3buka/go-rpc/auth/rpc/grpc/proto_user"
)

type Services struct {
	User uservice.Userer
	Auth aservice.Auther
}

func NewServicesRPS(storages *storages.Storages, components *component.Components) *Services {
	//userService := uservice.NewUserService(storages.User, components.Logger)
	userRPC := client.NewJSONRPC(components.Conf.UserRPC, components.Logger)
	userClientRPC := uservice.NewUserServiceJSONRPC(userRPC)
	return &Services{
		User: userClientRPC,
		Auth: aservice.NewAuth(userClientRPC, storages.Verify, components),
	}
}

func NewServicesGRPS(storages *storages.Storages, components *component.Components) *Services {
	userGRPS := client.NewJSONGRPC(components.Conf.UserRPC, components.Logger)
	userGRPSClient := proto_user.NewUserServiceRPCClient(userGRPS)
	userService := uservice.NewExchangeServiceGRPC(userGRPSClient)

	return &Services{
		User: userService,
		Auth: aservice.NewAuth(userService, storages.Verify, components),
	}
}
