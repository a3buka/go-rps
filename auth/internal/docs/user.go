package docs

import (
	ucontroller "gitlab.com/a3buka/go-rpc/auth/export/controller"
)

// swagger:route GET /api/1/auth/profile auth profileRequest
// Получение информации о текущем пользователе.
// security:
//   - Bearer: []
// responses:
//   200: profileResponse

// swagger:response profileResponse
type profileResponse struct {
	// in:body
	Body ucontroller.ProfileResponse
}
