package modules

import (
	"gitlab.com/a3buka/go-rpc/user/internal/infrastructure/component"

	"gitlab.com/a3buka/go-rpc/user/internal/modules/user/service"
	"gitlab.com/a3buka/go-rpc/user/internal/storages"
)

type Services struct {
	User service.Userer
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	return &Services{

		User: service.NewUserService(storages.User, components.Logger),
	}
}
