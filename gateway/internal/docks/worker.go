package docs

import "gitlab.com/a3buka/go-rpc/gateway/internal/modules/worker/controlls"

//go:generate swagger generate spec -o ../../static/swagger.json --scan-models

// swagger:route GET /api/1/worker/history worker workerHistoryRequest
// Хранение истории цен криптовалюты.
// security:
//   - Bearer: []
// responses:
//   200: workerHistoryResponse

// swagger:response workerHistoryResponse
//
//nolint:all
type workerHistoryResponse struct {
	// in:body
	Body []controlls.ProfileResponseHistory
}

// swagger:route GET /api/1/worker/max worker workerMaxRequest
// Получение списка пар криптовалют с максимальной ценой.
// security:
//   - Bearer: []
// responses:
//   200: workerMaxResponse

// swagger:response workerMaxResponse
//
//nolint:all
type workerMaxResponse struct {
	// in:body
	Body []controlls.ProfileResponseMax
}

// swagger:route GET /api/1/worker/min worker workerMinRequest
// Получение списка пар криптовалют с минимальной ценой.
// security:
//   - Bearer: []
// responses:
//   200: workerMinResponse

// swagger:response workerMinResponse
//
//nolint:all
type workerMinResponse struct {
	// in:body
	Body []controlls.ProfileResponseMin
}

// swagger:route GET /api/1/worker/avg worker workerAvgRequest
// Вывод списка пар криптовалют со средней ценой.
// security:
//   - Bearer: []
// responses:
//   200: workerAvgResponse

// swagger:response workerAvgResponse
//
//nolint:all
type workerAvgResponse struct {
	// in:body
	Body []controlls.ProfileResponseAvg
}
