package modules

import (
	"gitlab.com/a3buka/go-rpc/gateway/internal/infrastracture/components"
	acontroller "gitlab.com/a3buka/go-rpc/gateway/internal/modules/auth/controllers"
	ucontrollers "gitlab.com/a3buka/go-rpc/gateway/internal/modules/user/controllers"
	wcontrolls "gitlab.com/a3buka/go-rpc/gateway/internal/modules/worker/controlls"
)

type Controllers struct {
	Auth acontroller.Auther
	User ucontrollers.Userer
	Work wcontrolls.Workerer
}

func NewControllers(services *Services, components *components.Components) *Controllers {
	authController := acontroller.NewAuth(services.Auth, components)
	userController := ucontrollers.NewUser(services.User, components)
	workController := wcontrolls.NewWorker(services.Work, components)

	return &Controllers{
		Auth: authController,
		User: userController,
		Work: workController,
	}
}
