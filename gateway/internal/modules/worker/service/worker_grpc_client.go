package service

import (
	"context"
	errors "gitlab.com/a3buka/go-rpc/gateway/internal/infrastracture/error"
	"gitlab.com/a3buka/go-rpc/gateway/internal/models"
	"gitlab.com/a3buka/go-rpc/gateway/rpc/proto_worker"
)

type ExchangeServiceGRPCClient struct {
	client proto_worker.ExchangeServiceRPCClient
}

func (e *ExchangeServiceGRPCClient) MinPrices(ctx context.Context) WorkerMinPriceOut {
	req, err := e.client.Min(ctx, &proto_worker.BaseRequest{})
	if err != nil {
		return WorkerMinPriceOut{
			Succses:   false,
			ErrorCode: 400,
			Worker:    []models.WorkerMinPrice{},
		}
	}
	res := make([]models.WorkerMinPrice, len(req.Coins))
	for idx, value := range req.Coins {
		res[idx] = models.WorkerMinPrice{
			Name: value.Name,
			Low:  value.Low,
		}
	}
	return WorkerMinPriceOut{
		Succses:   req.Success,
		ErrorCode: errors.NoError,
		Worker:    res,
	}
}

func (e *ExchangeServiceGRPCClient) MaxPrices(ctx context.Context) WorkerMaxPriceOut {
	req, err := e.client.Max(ctx, &proto_worker.BaseRequest{})
	if err != nil {
		return WorkerMaxPriceOut{
			Succses:   false,
			ErrorCode: 400,
			Worker:    []models.WorkerMaxPrice{},
		}
	}
	res := make([]models.WorkerMaxPrice, len(req.Coins))
	for idx, value := range req.Coins {
		res[idx] = models.WorkerMaxPrice{
			Name: value.Name,
			High: value.High,
		}
	}
	return WorkerMaxPriceOut{
		Succses:   req.Success,
		ErrorCode: errors.NoError,
		Worker:    res,
	}
}

func (e *ExchangeServiceGRPCClient) AvgPrices(ctx context.Context) WorkerAvgPriceOut {
	req, err := e.client.Avg(ctx, &proto_worker.BaseRequest{})
	if err != nil {
		return WorkerAvgPriceOut{
			Succses:   false,
			ErrorCode: 400,
			Worker:    []models.WorkerAvgPrice{},
		}
	}
	res := make([]models.WorkerAvgPrice, len(req.Coins))
	for idx, value := range req.Coins {
		res[idx] = models.WorkerAvgPrice{
			Name: value.Name,
			Avg:  value.Avg,
		}
	}
	return WorkerAvgPriceOut{
		Succses:   req.Success,
		ErrorCode: errors.NoError,
		Worker:    res,
	}
}

func (e *ExchangeServiceGRPCClient) History(ctx context.Context) WorkerHistory {
	req, err := e.client.History(ctx, &proto_worker.BaseRequest{})
	if err != nil {
		return WorkerHistory{
			Succses:   false,
			ErrorCode: 400,
			History:   []models.WorkerDTO{},
		}
	}
	res := make([]models.WorkerDTO, len(req.WorkerDtos))
	for idx, value := range req.WorkerDtos {
		res[idx] = models.WorkerDTO{
			ID:        int(value.Id),
			Name:      value.Name,
			BuyPrice:  value.BuyPrice,
			SellPrice: value.SellPrice,
			LastTrade: value.LastTrade,
			High:      value.High,
			Low:       value.Low,
			Avg:       value.Avg,
			Vol:       value.Vol,
			VolCurr:   value.VolCurr,
			Updated:   int(value.Updated),
		}
	}
	return WorkerHistory{
		Succses:   req.Success,
		ErrorCode: errors.NoError,
		History:   res,
	}
}

func NewExchangeServiceGRPC(client proto_worker.ExchangeServiceRPCClient) *ExchangeServiceGRPCClient {
	a := &ExchangeServiceGRPCClient{client: client}
	return a
}
