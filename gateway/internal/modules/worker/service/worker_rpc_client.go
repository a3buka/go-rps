package service

import (
	"context"
	"gitlab.com/a3buka/go-rpc/gateway/internal/infrastracture/client"
	"log"
)

type WorkerServerJSONRPC struct {
	client client.Client
}

var Empty struct{}

func (w *WorkerServerJSONRPC) MinPrices(ctx context.Context) WorkerMinPriceOut {
	var out WorkerMinPriceOut
	err := w.client.Call("WorkerServerJSONRPC.MinPrice", Empty, &out)
	if err != nil {
		log.Fatal(err)
	}
	return out
}

func (w *WorkerServerJSONRPC) MaxPrices(ctx context.Context) WorkerMaxPriceOut {
	var out WorkerMaxPriceOut
	err := w.client.Call("WorkerServerJSONRPC.MaxPrice", Empty, &out)
	if err != nil {
		log.Fatal(err)
	}
	return out
}

func (w *WorkerServerJSONRPC) AvgPrices(ctx context.Context) WorkerAvgPriceOut {
	var out WorkerAvgPriceOut
	err := w.client.Call("WorkerServerJSONRPC.MaxPrice", Empty, &out)
	if err != nil {
		log.Fatal(err)
	}
	return out
}

func (w *WorkerServerJSONRPC) History(ctx context.Context) WorkerHistory {
	var out WorkerHistory
	err := w.client.Call("WorkerServerJSONRPC.MaxPrice", Empty, &out)
	if err != nil {
		log.Fatal(err)
	}
	return out
}

func NewWorkServiceJsonRPC(client client.Client) *WorkerServerJSONRPC {
	return &WorkerServerJSONRPC{client: client}
}
