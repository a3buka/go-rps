package modules

import (
	"gitlab.com/a3buka/go-rpc/gateway/internal/infrastracture/client"
	"gitlab.com/a3buka/go-rpc/gateway/internal/infrastracture/components"
	aservcie "gitlab.com/a3buka/go-rpc/gateway/internal/modules/auth/service"
	uservice "gitlab.com/a3buka/go-rpc/gateway/internal/modules/user/service"
	wservice "gitlab.com/a3buka/go-rpc/gateway/internal/modules/worker/service"
	"gitlab.com/a3buka/go-rpc/gateway/rpc/proto_auth"
	"gitlab.com/a3buka/go-rpc/gateway/rpc/proto_user"
	"gitlab.com/a3buka/go-rpc/gateway/rpc/proto_worker"
)

type Services struct {
	User uservice.Userer
	Auth aservcie.Auther
	Work wservice.Workerer
}

func NewServicesJRPC(components *components.Components) *Services {
	var authClient client.Client
	var userClient client.Client
	var workClient client.Client

	authClient = client.NewJSONRPC(components.Conf.AuthRPC, components.Logger)
	userClient = client.NewJSONRPC(components.Conf.UserRPC, components.Logger)
	workClient = client.NewJSONRPC(components.Conf.WorkerRPC, components.Logger)

	authClientRPC := aservcie.NewAuthServiceJSONRPC(authClient)
	userClientRPC := uservice.NewUserServiceJSONRPC(userClient)
	workClientRPC := wservice.NewWorkServiceJsonRPC(workClient)

	return &Services{

		User: userClientRPC,
		Auth: authClientRPC,
		Work: workClientRPC,
	}
}

func NewServicesGRPC(components *components.Components) *Services {
	authGRPS := client.NewJSONGRPC(components.Conf.AuthRPC, components.Logger)
	authClient := proto_auth.NewAuthServiceRPCClient(authGRPS)
	authService := aservcie.NewExchangeServiceGRPC(authClient)

	workerGRPS := client.NewJSONGRPC(components.Conf.WorkerRPC, components.Logger)
	workerClient := proto_worker.NewExchangeServiceRPCClient(workerGRPS)
	workerService := wservice.NewExchangeServiceGRPC(workerClient)

	userGRPS := client.NewJSONGRPC(components.Conf.UserRPC, components.Logger)
	userClient := proto_user.NewUserServiceRPCClient(userGRPS)
	userService := uservice.NewExchangeServiceGRPC(userClient)

	return &Services{
		Work: workerService,
		Auth: authService,
		User: userService,
	}

}
